﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace App13
{
	public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();

			MainPage = new App13.MainPage();
		}

		protected override void OnStart ()
		{
            AppCenter.Start("2210139d-af3d-4b50-ba9c-d1e669fbb2b6",
                      typeof(Analytics), typeof(Crashes));
            // Handle when your app starts
        }

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
